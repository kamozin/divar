<?php

/* E:\OpenServer\domains\logist/themes/demo/pages/novosti.htm */
class __TwigTemplate_d01dc57645277656ec90c28b4aa6e7d93adf24b366f5c35d12233ecd087f1725 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<style>
    .posts {
        margin-bottom: 30px;
    }
    .image-news {
        width: 200px;
    }

    .link-image {
        display: block;
    }

    .link-image img {
        max-width: 100%;
    }
    .title h1{
        margin: 0;
    }
    .dt{
        color: #357cd1;
        padding: 10px;
    }

    .dt span {

    }
</style>
<div class=\"LayoutMain\">
    <main class=\"Main\" role=\"main\">
        <article class=\"Post\">
            <header class=\"Post-header\">
                <h1 class=\"Post-title container\">Новости</h1>
            </header>

            <div class=\"Post-content container\">

                <div class=\"posts\">
                    <div class=\"item-posts\">
                        <div class=\"row\">

                            <div class=\"image-news col-md-3\">
                                <a class=\"link-image\" href=\"\">
                                    <img src=\"http://via.placeholder.com/200x150\" alt=\"\">
                                </a>
                            </div>
                            <div class=\"col-md-9\">
                                <div class=\"title\">
                                    <h1>Новость 1</h1>
                                </div>
                                <div class=\"dt\">
                                   <span>Дата: </span> 22.10.2017
                                </div>

                                <div class=\"text-post\">
                                    <p>Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне.
                                        Lorem Ipsum является0стандартной \"рыбой\" для текстов на латинице с начала XVI века.
                                        В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum
                                        для распечатки образцов.
                                        Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн.</p>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class=\"item-posts\">
                        <div class=\"row\">

                            <div class=\"image-news col-md-3\">
                                <a class=\"link-image\" href=\"\">
                                    <img src=\"http://via.placeholder.com/200x150\" alt=\"\">
                                </a>
                            </div>
                            <div class=\"col-md-9\">
                                <div class=\"title\">
                                    <h1>Новость 2</h1>
                                </div>
                                <div class=\"dt\">
                                    <span>Дата: </span> 22.10.2017
                                </div>

                                <div class=\"text-post\">
                                    <p>Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне.
                                        Lorem Ipsum является0стандартной \"рыбой\" для текстов на латинице с начала XVI века.
                                        В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum
                                        для распечатки образцов.
                                        Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн.</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </article>

    </main>
</div>";
    }

    public function getTemplateName()
    {
        return "E:\\OpenServer\\domains\\logist/themes/demo/pages/novosti.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<style>
    .posts {
        margin-bottom: 30px;
    }
    .image-news {
        width: 200px;
    }

    .link-image {
        display: block;
    }

    .link-image img {
        max-width: 100%;
    }
    .title h1{
        margin: 0;
    }
    .dt{
        color: #357cd1;
        padding: 10px;
    }

    .dt span {

    }
</style>
<div class=\"LayoutMain\">
    <main class=\"Main\" role=\"main\">
        <article class=\"Post\">
            <header class=\"Post-header\">
                <h1 class=\"Post-title container\">Новости</h1>
            </header>

            <div class=\"Post-content container\">

                <div class=\"posts\">
                    <div class=\"item-posts\">
                        <div class=\"row\">

                            <div class=\"image-news col-md-3\">
                                <a class=\"link-image\" href=\"\">
                                    <img src=\"http://via.placeholder.com/200x150\" alt=\"\">
                                </a>
                            </div>
                            <div class=\"col-md-9\">
                                <div class=\"title\">
                                    <h1>Новость 1</h1>
                                </div>
                                <div class=\"dt\">
                                   <span>Дата: </span> 22.10.2017
                                </div>

                                <div class=\"text-post\">
                                    <p>Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне.
                                        Lorem Ipsum является0стандартной \"рыбой\" для текстов на латинице с начала XVI века.
                                        В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum
                                        для распечатки образцов.
                                        Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн.</p>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class=\"item-posts\">
                        <div class=\"row\">

                            <div class=\"image-news col-md-3\">
                                <a class=\"link-image\" href=\"\">
                                    <img src=\"http://via.placeholder.com/200x150\" alt=\"\">
                                </a>
                            </div>
                            <div class=\"col-md-9\">
                                <div class=\"title\">
                                    <h1>Новость 2</h1>
                                </div>
                                <div class=\"dt\">
                                    <span>Дата: </span> 22.10.2017
                                </div>

                                <div class=\"text-post\">
                                    <p>Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне.
                                        Lorem Ipsum является0стандартной \"рыбой\" для текстов на латинице с начала XVI века.
                                        В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum
                                        для распечатки образцов.
                                        Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн.</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </article>

    </main>
</div>", "E:\\OpenServer\\domains\\logist/themes/demo/pages/novosti.htm", "");
    }
}
