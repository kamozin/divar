<?php

/* E:\OpenServer\domains\logist/themes/demo/pages/kontakty.htm */
class __TwigTemplate_bbb43d2ce4edb70ef95bb1473a47157e52ea455bb83a4cf60ed2a92fafe75db2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"margin-bottom: 20px\" class=\"LayoutMain\">
    <main class=\"Main\" role=\"main\">
      <article class=\"Post\">
        <header class=\"Post-header\">
          <h1 class=\"Post-title container\">Контакты</h1>
        </header>

        <div class=\"Post-content container\">
          <div class=\"row\">
            <div class=\"col-md-6\">
              <div class=\"ContactsMap\" style=\"height: 400px;\" id=\"ContactsMap\">
                  <script type=\"text/javascript\" charset=\"utf-8\" async src=\"https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa900b931a273871a1e572774018ade13e6eda3f69ea8d6715d102aaf30f0d512&amp;width=600&amp;height=400&amp;lang=ru_RU&amp;scroll=true\"></script>
              </div>
            </div>
            <div class=\"col-md-6\">
              <ul class=\"ContactsInfo\">
                <li class=\"ContactsInfo-item\">
                  <b>Адрес:</b> г. Брянск...
                </li>
                <li class=\"ContactsInfo-item\">
                  <b>Офис:</b> <a href=\"tel:+0000000\">0000000</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </article>

    </main>
  </div>";
    }

    public function getTemplateName()
    {
        return "E:\\OpenServer\\domains\\logist/themes/demo/pages/kontakty.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div style=\"margin-bottom: 20px\" class=\"LayoutMain\">
    <main class=\"Main\" role=\"main\">
      <article class=\"Post\">
        <header class=\"Post-header\">
          <h1 class=\"Post-title container\">Контакты</h1>
        </header>

        <div class=\"Post-content container\">
          <div class=\"row\">
            <div class=\"col-md-6\">
              <div class=\"ContactsMap\" style=\"height: 400px;\" id=\"ContactsMap\">
                  <script type=\"text/javascript\" charset=\"utf-8\" async src=\"https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa900b931a273871a1e572774018ade13e6eda3f69ea8d6715d102aaf30f0d512&amp;width=600&amp;height=400&amp;lang=ru_RU&amp;scroll=true\"></script>
              </div>
            </div>
            <div class=\"col-md-6\">
              <ul class=\"ContactsInfo\">
                <li class=\"ContactsInfo-item\">
                  <b>Адрес:</b> г. Брянск...
                </li>
                <li class=\"ContactsInfo-item\">
                  <b>Офис:</b> <a href=\"tel:+0000000\">0000000</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </article>

    </main>
  </div>", "E:\\OpenServer\\domains\\logist/themes/demo/pages/kontakty.htm", "");
    }
}
