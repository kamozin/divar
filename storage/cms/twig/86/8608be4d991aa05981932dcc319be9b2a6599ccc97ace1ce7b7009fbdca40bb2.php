<?php

/* E:\OpenServer\domains\logist/themes/demo/layouts/default.htm */
class __TwigTemplate_f3af8f7637451ccf4a86bb2d4a54eb5f2888d45ba412fa3031156559f939fb8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 8]>         <html class=\"no-js lt-ie9\" lang=\"ru\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\" lang=\"ru\"> <!--<![endif]-->
<head>
    <meta charset=\"utf-8\">

    <title>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</title>
    <link rel=\"shortcut icon\" href=\"";
        // line 8
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/d/images/favicon.ico");
        echo "\" type=\"image/x-icon\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"description\" content=\"\">
    <meta name=\"keywords\" content=\"\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300,700|Roboto+Condensed:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href=\"";
        // line 15
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/d/css/main.css?v=1.0.1");
        echo "\" rel=\"stylesheet\">
    ";
        // line 16
        echo $this->env->getExtension('CMS')->assetsFunction('css');
        echo $this->env->getExtension('CMS')->displayBlock('styles');
        // line 17
        echo "    <!-- HTML5 shim, for IE6 to IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src=\"//html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->
</head>
<body class=\"home\">
<!--[if lt IE 9]>
<p class=\"browsehappy\">Вы пользуетесь <strong>устаревшим</strong> браузером. Пожалуйста, <a href=\"http://browsehappy.com/\">обновите ваш браузер</a>.</p>
<![endif]-->

<!-- load combined svg file (with symbols) into body-->
<script>
    (function (doc) {
        var scripts = doc.getElementsByTagName('script');
        var script = scripts[scripts.length - 1];
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            var div = doc.createElement('div');
            div.innerHTML = this.responseText;
            div.style.display = 'none';
            script.parentNode.insertBefore(div, script);
        }
        xhr.open('get', '/themes/demo/assets/d/images/svg_icons.svg', true);
        xhr.send();
    })(document);
</script>

<div class=\"LayoutHeader\">
    <header class=\"Header\" role=\"banner\">
        <div class=\"LayoutTopBar\">
            <div class=\"TopBar\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-5 col-xl-4\">
                            <a style=\"text-decoration: none;\" href=\"/\" class=\"Brand\">
                                <img src=\"";
        // line 52
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/d/images/ved2.jpg");
        echo "\" alt=\"ДИВАР Ф.К.\" class=\"Brand-logo\">
                                <span class=\"Brand-text\">ДИВАР Ф.К.</span>
                            </a>
                        </div>
                        <div class=\"col-md-7 col-xl-8\">
                            <a href=\"#mobile-nav\" class=\"Button Menu-trigger hidden-md-up\">
                                <div class=\"Iconizer\">
                                    <div class=\"Iconizer-slot\">
                                        <div class=\"Iconizer-picture\">
                                            <svg class=\"icon\">
                                                <use xlink:href=\"#icon-menu\"></use>
                                            </svg>
                                        </div>
                                    </div>
                    <span class=\"Iconizer-slot\">
                      <span class=\"Iconizer-content\">Меню</span>
                    </span>
                                </div>
                            </a>
                            <div class=\"Contact Contact--header\">
                                <div class=\"Contact-line\">
                                    <div class=\"Contact-phone\">
                                        <div class=\"Iconizer\">
                                            <div class=\"Iconizer-slot\">
                                                <div class=\"Iconizer-picture\">
                                                    <svg class=\"icon\">
                                                        <use xlink:href=\"#icon-phone\"></use>
                                                    </svg>
                                                </div>
                                            </div>
                        <span class=\"Iconizer-slot\">
                          <span class=\"Iconizer-content\"><a href=\"tel:8-950-695-61-77\">8-800-200-0000</a></span>
                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"Contact-line\">
                                    <a href=\"#contact-form-popup\" class=\"Contact-button js-openPopupContactForm\">Заказать <span class=\"hidden-md-down\">обратный</span> звонок</a>
                                </div>
                            </div>
                            <nav class=\"MainNav hidden-md-down\" role=\"navigation\">
                                <ul class=\"MainNav-list\">
                                    <li class=\"MainNav-item\"><a href=\"/\" class=\"MainNav-link\">
                                        Главная</a></li>
                                    <li class=\"MainNav-item\"><a href=\"/about\" class=\"MainNav-link\">О нас</a></li>
                                    <li class=\"MainNav-item\"><a href=\"/services\" class=\"MainNav-link\">Услуги</a></li>
                                    <li class=\"MainNav-item\"><a href=\"/news\" class=\"MainNav-link\">Новости</a></li>
                                    <li class=\"MainNav-item\"><a href=\"/contact\" class=\"MainNav-link\">Контакты</a></li>
                                </ul>
                            </nav>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </header>
</div>


";
        // line 113
        echo $this->env->getExtension('CMS')->pageFunction();
        // line 114
        echo "

<div class=\"LayoutFooter\">
    <footer class=\"Footer\" role=\"contentinfo\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-6\">

                    <div class=\"Brand\">
                        <div class=\"Brand-logo\">
                            <img src=\"";
        // line 124
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/d/images/ved3.png");
        echo "\" alt=\"ДИВАР Ф.К.\">
                        </div>
                    </div>

                </div>

                <div class=\"col-sm-6\">

                    <div class=\"Contact\">
                        <div class=\"Contact-line\">
                            <div class=\"Contact-phone\">
                  <span class=\"Iconizer Iconizer--row\">
                    <span class=\"Iconizer-slot\">
                      <span class=\"Iconizer-picture\">
                        <svg class=\"icon\">
                            <use xlink:href=\"#icon-phone\"></use>
                        </svg>
                      </span>
                    </span>
                    <span class=\"Iconizer-slot\">
                      <span class=\"Iconizer-content\">8-800-200-0000</span>
                    </span>
                  </span>
                            </div>
                        </div>
                        <div class=\"Contact-line\">
                            <a href=\"#contact-form-popup\" class=\"Contact-button js-openPopupContactForm Button--fluid\">Заказать <span class=\"hidden-md-down\">обратный</span> звонок</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </footer>
</div>


<nav class=\"MobileMainNav\" id=\"mobile-nav\" role=\"navigation\">
    <ul class=\"MobileMainNav-list\">
        <li
                class=\"MobileMainNav-item MobileMainNav-item-isActive\"><a href=\"/\" class=\"MobileMainNav-link\">Главная</a></li>
        <li class=\"MobileMainNav-item\"><a href=\"/about\" class=\"MobileMainNav-link\">О нас</a></li>
        <li class=\"MobileMainNav-item\"><a href=\"/services\" class=\"MobileMainNav-link\">Услуги</a></li>
        <li class=\"MobileMainNav-item\"><a href=\"/news\" class=\"MobileMainNav-link\">Новости</a></li>
        <li class=\"MobileMainNav-item\"><a href=\"/contact\" class=\"MobileMainNav-link\">Контакты</a></li>
    </ul>
</nav>


<div id=\"contact-form-popup\" class=\"mfp-hide Popup\">
    <form action=\"\" method=\"post\" class=\"Popup-contact-form\">
        <fieldset>
            <legend>Заказать бесплатный звонок</legend>
            <div class=\"Form-group\">
                <input type=\"text\" class=\"Form-control\" placeholder=\"Имя\" required>
            </div>
            <div class=\"Form-group\">
                <input type=\"text\" id=\"phone\" class=\"Form-control\" placeholder=\"Телефон\" required>
            </div>
            <div class=\"text-center\">
                <button class=\"Button\" type=\"submit\">Отправить</button>
            </div>
        </fieldset>
    </form>
</div>

<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js\"></script>
<script>window.jQuery || document.write('<script src=\"js/libs/jquery-1.12.2.min.js\"><\\/script>')</script>
<script src=\"";
        // line 192
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/d/js/sweetalert/sweetalert.min.js");
        echo "\"></script>
<script src=\"";
        // line 193
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/d/js/jquery.maskedinput.min.js");
        echo "\"></script>
<script src=\"";
        // line 194
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/d/js/plugins.js");
        echo "\"></script>
<script src=\"";
        // line 195
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/d/js/jquery.inputmask.js");
        echo "\"></script>
<script src=\"";
        // line 196
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/d/js/site.js?v=1.0.2");
        echo "\"></script>
";
        // line 197
        echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
        echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras.css">'.PHP_EOL;
        // line 198
        echo $this->env->getExtension('CMS')->assetsFunction('js');
        echo $this->env->getExtension('CMS')->displayBlock('scripts');
        // line 199
        echo "
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "E:\\OpenServer\\domains\\logist/themes/demo/layouts/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  264 => 199,  261 => 198,  254 => 197,  250 => 196,  246 => 195,  242 => 194,  238 => 193,  234 => 192,  163 => 124,  151 => 114,  149 => 113,  85 => 52,  48 => 17,  45 => 16,  41 => 15,  31 => 8,  27 => 7,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<!--[if IE 8]>         <html class=\"no-js lt-ie9\" lang=\"ru\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\" lang=\"ru\"> <!--<![endif]-->
<head>
    <meta charset=\"utf-8\">

    <title>{{ this.page.title }}</title>
    <link rel=\"shortcut icon\" href=\"{{'assets/d/images/favicon.ico'|theme }}\" type=\"image/x-icon\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"description\" content=\"\">
    <meta name=\"keywords\" content=\"\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300,700|Roboto+Condensed:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href=\"{{'assets/d/css/main.css?v=1.0.1'|theme }}\" rel=\"stylesheet\">
    {% styles %}
    <!-- HTML5 shim, for IE6 to IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src=\"//html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]-->
</head>
<body class=\"home\">
<!--[if lt IE 9]>
<p class=\"browsehappy\">Вы пользуетесь <strong>устаревшим</strong> браузером. Пожалуйста, <a href=\"http://browsehappy.com/\">обновите ваш браузер</a>.</p>
<![endif]-->

<!-- load combined svg file (with symbols) into body-->
<script>
    (function (doc) {
        var scripts = doc.getElementsByTagName('script');
        var script = scripts[scripts.length - 1];
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            var div = doc.createElement('div');
            div.innerHTML = this.responseText;
            div.style.display = 'none';
            script.parentNode.insertBefore(div, script);
        }
        xhr.open('get', '/themes/demo/assets/d/images/svg_icons.svg', true);
        xhr.send();
    })(document);
</script>

<div class=\"LayoutHeader\">
    <header class=\"Header\" role=\"banner\">
        <div class=\"LayoutTopBar\">
            <div class=\"TopBar\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-5 col-xl-4\">
                            <a style=\"text-decoration: none;\" href=\"/\" class=\"Brand\">
                                <img src=\"{{'assets/d/images/ved2.jpg'|theme }}\" alt=\"ДИВАР Ф.К.\" class=\"Brand-logo\">
                                <span class=\"Brand-text\">ДИВАР Ф.К.</span>
                            </a>
                        </div>
                        <div class=\"col-md-7 col-xl-8\">
                            <a href=\"#mobile-nav\" class=\"Button Menu-trigger hidden-md-up\">
                                <div class=\"Iconizer\">
                                    <div class=\"Iconizer-slot\">
                                        <div class=\"Iconizer-picture\">
                                            <svg class=\"icon\">
                                                <use xlink:href=\"#icon-menu\"></use>
                                            </svg>
                                        </div>
                                    </div>
                    <span class=\"Iconizer-slot\">
                      <span class=\"Iconizer-content\">Меню</span>
                    </span>
                                </div>
                            </a>
                            <div class=\"Contact Contact--header\">
                                <div class=\"Contact-line\">
                                    <div class=\"Contact-phone\">
                                        <div class=\"Iconizer\">
                                            <div class=\"Iconizer-slot\">
                                                <div class=\"Iconizer-picture\">
                                                    <svg class=\"icon\">
                                                        <use xlink:href=\"#icon-phone\"></use>
                                                    </svg>
                                                </div>
                                            </div>
                        <span class=\"Iconizer-slot\">
                          <span class=\"Iconizer-content\"><a href=\"tel:8-950-695-61-77\">8-800-200-0000</a></span>
                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"Contact-line\">
                                    <a href=\"#contact-form-popup\" class=\"Contact-button js-openPopupContactForm\">Заказать <span class=\"hidden-md-down\">обратный</span> звонок</a>
                                </div>
                            </div>
                            <nav class=\"MainNav hidden-md-down\" role=\"navigation\">
                                <ul class=\"MainNav-list\">
                                    <li class=\"MainNav-item\"><a href=\"/\" class=\"MainNav-link\">
                                        Главная</a></li>
                                    <li class=\"MainNav-item\"><a href=\"/about\" class=\"MainNav-link\">О нас</a></li>
                                    <li class=\"MainNav-item\"><a href=\"/services\" class=\"MainNav-link\">Услуги</a></li>
                                    <li class=\"MainNav-item\"><a href=\"/news\" class=\"MainNav-link\">Новости</a></li>
                                    <li class=\"MainNav-item\"><a href=\"/contact\" class=\"MainNav-link\">Контакты</a></li>
                                </ul>
                            </nav>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </header>
</div>


{% page %}


<div class=\"LayoutFooter\">
    <footer class=\"Footer\" role=\"contentinfo\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-6\">

                    <div class=\"Brand\">
                        <div class=\"Brand-logo\">
                            <img src=\"{{'assets/d/images/ved3.png'|theme }}\" alt=\"ДИВАР Ф.К.\">
                        </div>
                    </div>

                </div>

                <div class=\"col-sm-6\">

                    <div class=\"Contact\">
                        <div class=\"Contact-line\">
                            <div class=\"Contact-phone\">
                  <span class=\"Iconizer Iconizer--row\">
                    <span class=\"Iconizer-slot\">
                      <span class=\"Iconizer-picture\">
                        <svg class=\"icon\">
                            <use xlink:href=\"#icon-phone\"></use>
                        </svg>
                      </span>
                    </span>
                    <span class=\"Iconizer-slot\">
                      <span class=\"Iconizer-content\">8-800-200-0000</span>
                    </span>
                  </span>
                            </div>
                        </div>
                        <div class=\"Contact-line\">
                            <a href=\"#contact-form-popup\" class=\"Contact-button js-openPopupContactForm Button--fluid\">Заказать <span class=\"hidden-md-down\">обратный</span> звонок</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </footer>
</div>


<nav class=\"MobileMainNav\" id=\"mobile-nav\" role=\"navigation\">
    <ul class=\"MobileMainNav-list\">
        <li
                class=\"MobileMainNav-item MobileMainNav-item-isActive\"><a href=\"/\" class=\"MobileMainNav-link\">Главная</a></li>
        <li class=\"MobileMainNav-item\"><a href=\"/about\" class=\"MobileMainNav-link\">О нас</a></li>
        <li class=\"MobileMainNav-item\"><a href=\"/services\" class=\"MobileMainNav-link\">Услуги</a></li>
        <li class=\"MobileMainNav-item\"><a href=\"/news\" class=\"MobileMainNav-link\">Новости</a></li>
        <li class=\"MobileMainNav-item\"><a href=\"/contact\" class=\"MobileMainNav-link\">Контакты</a></li>
    </ul>
</nav>


<div id=\"contact-form-popup\" class=\"mfp-hide Popup\">
    <form action=\"\" method=\"post\" class=\"Popup-contact-form\">
        <fieldset>
            <legend>Заказать бесплатный звонок</legend>
            <div class=\"Form-group\">
                <input type=\"text\" class=\"Form-control\" placeholder=\"Имя\" required>
            </div>
            <div class=\"Form-group\">
                <input type=\"text\" id=\"phone\" class=\"Form-control\" placeholder=\"Телефон\" required>
            </div>
            <div class=\"text-center\">
                <button class=\"Button\" type=\"submit\">Отправить</button>
            </div>
        </fieldset>
    </form>
</div>

<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js\"></script>
<script>window.jQuery || document.write('<script src=\"js/libs/jquery-1.12.2.min.js\"><\\/script>')</script>
<script src=\"{{ 'assets/d/js/sweetalert/sweetalert.min.js'|theme }}\"></script>
<script src=\"{{ 'assets/d/js/jquery.maskedinput.min.js'|theme }}\"></script>
<script src=\"{{ 'assets/d/js/plugins.js'|theme }}\"></script>
<script src=\"{{ 'assets/d/js/jquery.inputmask.js'|theme }}\"></script>
<script src=\"{{ 'assets/d/js/site.js?v=1.0.2'|theme }}\"></script>
{% framework extras %}
{% scripts %}

</body>
</html>", "E:\\OpenServer\\domains\\logist/themes/demo/layouts/default.htm", "");
    }
}
