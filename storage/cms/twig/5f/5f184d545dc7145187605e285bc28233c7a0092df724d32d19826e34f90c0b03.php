<?php

/* E:\OpenServer\domains\logist/themes/demo/pages/home.htm */
class __TwigTemplate_4dcfb95351b0afc385753f1cdaf9c3631792b010ae0dd0c83e236b2d34ef65a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"LayoutHeroSlider\">
    ";
        // line 2
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("slider"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 3
        echo "</div>
<div class=\"LayoutMain\">
    <main class=\"Main\" role=\"main\">
        <section class=\"SectionHome SectionHomeWhat\">
            <div class=\"container\">
                <header class=\"SectionHome-header\">
                    <h2 class=\"SectionHome-title\">Что мы предлагаем?</h2>
                </header>

                <div class=\"SectionHome-content\">
                    <ul>
                        <li>Консалтинг, финансовая логистика</li>
                        <li>Полная финансовая ответственность за товар</li>
                        <li>Оформление необходимой документации на груз</li>
                        <li>Сопровождение международных контрактов экспорта и импорта</li>
                        <li>Обработка грузов на складах</li>
                        <li>Таможенно-брокерские услуги</li>
                        <li>Услуги таможенного склада временного хранения</li>
                        <li>Прием, консолидация и хранение транзитных и других грузов на СВХ</li>
                        <li>Определение кода по ТН ВЭД СНГ, определение тарифной ставки по коду ТН ВЭД СНГ</li>
                        <li>Транспортные услуги</li>
                        <li>Экспедирование грузов</li>
                        <li>Консолидация грузов</li>
                    </ul>
                </div>

            </div>
        </section>





        <section class=\"SectionHome SectionHome--invertColor SectionHomeWhy\">

            <aside class=\"SectionHome SectionHome--invertColor SectionHomeCTA\">
                <div class=\"container \">
                    <form action=\"\" method=\"post\" class=\"Form FormCTA\">
                        <fieldset>
                            <legend>Заказать обратный звонок</legend>
                            <div class=\"row\">
                                <div class=\"col-md-4\">
                                    <div class=\"Form-group\">
                                        <label for=\"client_name\" class=\"Form-label\" title=\"Ваше имя\">Ваше имя:</label>
                                        <input type=\"text\" id=\"client_name\" name=\"name\" class=\"Form-control\"
                                               placeholder=\"Максим\" required>
                                    </div>
                                </div>
                                <div class=\"col-md-4\">
                                    <div class=\"Form-group\">
                                        <label for=\"client_phone\" class=\"Form-label\" title=\"Ваш телефон\">Ваш
                                            телефон:</label>
                                        <input type=\"tel\" id=\"client_phone\" name=\"phone\" class=\"Form-control\"
                                               placeholder=\"8 952 965 98 77\" required>
                                    </div>
                                </div>
                                <div class=\"col-md-3\">
                                    <button class=\"Button Form-button Button--fluid\" type=\"button\">Отправить</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </aside>


            <div class=\"container\">
                <header class=\"SectionHome-header\">
                    <h2 class=\"SectionHome-title\">Почему нам доверяют?</h2>
                </header>

                <div class=\"SectionHome-content\">

                    <div class=\"row\">
                        <div class=\"col-md-3\"><strong>Lorem ipsum dolor sit amet, pri oratio blandit ex, quod nisl mel ut. Erant facilisis mei cu, te vitae affert has, ne pro eligendi honestatis deterruisset.</strong></div>
                        <div class=\"col-md-3\"><strong>Lorem ipsum dolor sit amet, pri oratio blandit ex, quod nisl mel ut. Erant facilisis mei cu, te vitae affert has, ne pro eligendi honestatis deterruisset.</strong></div>
                        <div class=\"col-md-3\"><strong>Lorem ipsum dolor sit amet, pri oratio blandit ex, quod nisl mel ut. Erant facilisis mei cu, te vitae affert has, ne pro eligendi honestatis deterruisset.</strong></div>
                        <div class=\"col-md-3\"><strong>Lorem ipsum dolor sit amet, pri oratio blandit ex, quod nisl mel ut. Erant facilisis mei cu, te vitae affert has, ne pro eligendi honestatis deterruisset.</strong></div>
                    </div>
                </div>
            </div>
        </section>


    </main>
</div>";
    }

    public function getTemplateName()
    {
        return "E:\\OpenServer\\domains\\logist/themes/demo/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"LayoutHeroSlider\">
    {% component 'slider' %}
</div>
<div class=\"LayoutMain\">
    <main class=\"Main\" role=\"main\">
        <section class=\"SectionHome SectionHomeWhat\">
            <div class=\"container\">
                <header class=\"SectionHome-header\">
                    <h2 class=\"SectionHome-title\">Что мы предлагаем?</h2>
                </header>

                <div class=\"SectionHome-content\">
                    <ul>
                        <li>Консалтинг, финансовая логистика</li>
                        <li>Полная финансовая ответственность за товар</li>
                        <li>Оформление необходимой документации на груз</li>
                        <li>Сопровождение международных контрактов экспорта и импорта</li>
                        <li>Обработка грузов на складах</li>
                        <li>Таможенно-брокерские услуги</li>
                        <li>Услуги таможенного склада временного хранения</li>
                        <li>Прием, консолидация и хранение транзитных и других грузов на СВХ</li>
                        <li>Определение кода по ТН ВЭД СНГ, определение тарифной ставки по коду ТН ВЭД СНГ</li>
                        <li>Транспортные услуги</li>
                        <li>Экспедирование грузов</li>
                        <li>Консолидация грузов</li>
                    </ul>
                </div>

            </div>
        </section>





        <section class=\"SectionHome SectionHome--invertColor SectionHomeWhy\">

            <aside class=\"SectionHome SectionHome--invertColor SectionHomeCTA\">
                <div class=\"container \">
                    <form action=\"\" method=\"post\" class=\"Form FormCTA\">
                        <fieldset>
                            <legend>Заказать обратный звонок</legend>
                            <div class=\"row\">
                                <div class=\"col-md-4\">
                                    <div class=\"Form-group\">
                                        <label for=\"client_name\" class=\"Form-label\" title=\"Ваше имя\">Ваше имя:</label>
                                        <input type=\"text\" id=\"client_name\" name=\"name\" class=\"Form-control\"
                                               placeholder=\"Максим\" required>
                                    </div>
                                </div>
                                <div class=\"col-md-4\">
                                    <div class=\"Form-group\">
                                        <label for=\"client_phone\" class=\"Form-label\" title=\"Ваш телефон\">Ваш
                                            телефон:</label>
                                        <input type=\"tel\" id=\"client_phone\" name=\"phone\" class=\"Form-control\"
                                               placeholder=\"8 952 965 98 77\" required>
                                    </div>
                                </div>
                                <div class=\"col-md-3\">
                                    <button class=\"Button Form-button Button--fluid\" type=\"button\">Отправить</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </aside>


            <div class=\"container\">
                <header class=\"SectionHome-header\">
                    <h2 class=\"SectionHome-title\">Почему нам доверяют?</h2>
                </header>

                <div class=\"SectionHome-content\">

                    <div class=\"row\">
                        <div class=\"col-md-3\"><strong>Lorem ipsum dolor sit amet, pri oratio blandit ex, quod nisl mel ut. Erant facilisis mei cu, te vitae affert has, ne pro eligendi honestatis deterruisset.</strong></div>
                        <div class=\"col-md-3\"><strong>Lorem ipsum dolor sit amet, pri oratio blandit ex, quod nisl mel ut. Erant facilisis mei cu, te vitae affert has, ne pro eligendi honestatis deterruisset.</strong></div>
                        <div class=\"col-md-3\"><strong>Lorem ipsum dolor sit amet, pri oratio blandit ex, quod nisl mel ut. Erant facilisis mei cu, te vitae affert has, ne pro eligendi honestatis deterruisset.</strong></div>
                        <div class=\"col-md-3\"><strong>Lorem ipsum dolor sit amet, pri oratio blandit ex, quod nisl mel ut. Erant facilisis mei cu, te vitae affert has, ne pro eligendi honestatis deterruisset.</strong></div>
                    </div>
                </div>
            </div>
        </section>


    </main>
</div>", "E:\\OpenServer\\domains\\logist/themes/demo/pages/home.htm", "");
    }
}
