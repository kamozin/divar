<?php

/* E:\OpenServer\domains\logist/themes/demo/pages/uslugi.htm */
class __TwigTemplate_d48e4a1e4503ac17e7ec3062f6432f0695c9d5b6084ab6565dbfed048e7f1917 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"LayoutMain\">
    <main class=\"Main\" role=\"main\">
      <article class=\"Post\">
        <header class=\"Post-header\">
          <h1 class=\"Post-title container\">Услуги</h1>
        </header>

        <div class=\"Post-content container\">
          <div class=\"row\">

";
        // line 11
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("servicelist"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 12
        echo "          </div>
        </div>
      </article>

    </main>
  </div>";
    }

    public function getTemplateName()
    {
        return "E:\\OpenServer\\domains\\logist/themes/demo/pages/uslugi.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 12,  31 => 11,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"LayoutMain\">
    <main class=\"Main\" role=\"main\">
      <article class=\"Post\">
        <header class=\"Post-header\">
          <h1 class=\"Post-title container\">Услуги</h1>
        </header>

        <div class=\"Post-content container\">
          <div class=\"row\">

{% component 'servicelist' %}
          </div>
        </div>
      </article>

    </main>
  </div>", "E:\\OpenServer\\domains\\logist/themes/demo/pages/uslugi.htm", "");
    }
}
