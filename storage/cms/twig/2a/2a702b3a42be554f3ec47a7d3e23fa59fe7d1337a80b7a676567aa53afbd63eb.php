<?php

/* E:\OpenServer\domains\logist/plugins/kamozin/services/components/servieceslist/default.htm */
class __TwigTemplate_03be3eac88a10030abbd475a0f4dcb2493ade2d2758d9d4b30da9384c1db741e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

";
        // line 3
        $context["services"] = $this->getAttribute(($context["__SELF__"] ?? null), "services", array());
        // line 4
        echo "
";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["services"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["service"]) {
            // line 6
            echo "<div class=\"col-sm-6 col-md-4 col-lg-3 col-xxl-3\">
    <a href=\"/services/";
            // line 7
            echo twig_escape_filter($this->env, $this->getAttribute($context["service"], "slug", array()), "html", null, true);
            echo "\" class=\"ServiceShort\">
        <span class=\"ServiceShort-bg\" style=\"background-image: url('";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["service"], "img", array()), "path", array()), "html", null, true);
            echo "');\"></span>
        <h2 class=\"ServiceShort-title\">";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["service"], "title", array()), "html", null, true);
            echo "</h2>
    </a>
</div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['service'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "E:\\OpenServer\\domains\\logist/plugins/kamozin/services/components/servieceslist/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 9,  39 => 8,  35 => 7,  32 => 6,  28 => 5,  25 => 4,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("

{% set services = __SELF__.services %}

{% for service in services %}
<div class=\"col-sm-6 col-md-4 col-lg-3 col-xxl-3\">
    <a href=\"/services/{{service.slug}}\" class=\"ServiceShort\">
        <span class=\"ServiceShort-bg\" style=\"background-image: url('{{service.img.path}}');\"></span>
        <h2 class=\"ServiceShort-title\">{{service.title}}</h2>
    </a>
</div>

{% endfor %}", "E:\\OpenServer\\domains\\logist/plugins/kamozin/services/components/servieceslist/default.htm", "");
    }
}
