<?php namespace Kamozin\Services\Components;

use Cms\Classes\ComponentBase;
use Kamozin\Services\Models\Service as service;
class ServiecesList extends ComponentBase
{
    public $services;
    public function componentDetails()
    {
        return [
            'name'        => 'ServiecesList Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->services = $this->loadServices();
    }

    protected function loadServices(){
            $model =new service();

            $data=$model->get();

        return $data;
    }
}
