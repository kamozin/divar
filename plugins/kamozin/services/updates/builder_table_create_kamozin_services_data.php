<?php namespace Kamozin\Services\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKamozinServicesData extends Migration
{
    public function up()
    {
        Schema::create('kamozin_services_data', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('text');
            $table->string('slug');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kamozin_services_data');
    }
}
