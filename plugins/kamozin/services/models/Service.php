<?php namespace Kamozin\Services\Models;

use Model;

/**
 * Model
 */
class Service extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kamozin_services_data';

    public $attachOne=[
        'img'=>'System\Models\File'
    ];
}