<?php namespace Kamozin\Services;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\kamozin\Services\Components\ServiecesList'=>'servicelist',
            '\kamozin\services\Components\Serviece'=>'Service'
        ];
    }

    public function registerSettings()
    {
    }
}
