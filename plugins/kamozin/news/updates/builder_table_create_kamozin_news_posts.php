<?php namespace Kamozin\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKamozinNewsPosts extends Migration
{
    public function up()
    {
        Schema::create('kamozin_news_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('descriprion');
            $table->text('text');
            $table->string('slug');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kamozin_news_posts');
    }
}
