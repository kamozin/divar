<?php namespace Kamozin\News;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\kamozin\news\Components\Posts'=>'posts',
            '\kamozin\news\Components\Post'=>'post'
        ];
    }

    public function registerSettings()
    {
    }
}
