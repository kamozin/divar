
/*=================================================
 =            Translating magnificPopup            =
 =================================================*/

// Add it after jquery.magnific-popup.js and before first initialization code
$.extend(true, $.magnificPopup.defaults, {
  tClose: 'Закрыть (Esc)', // Alt text on close button
  tLoading: 'Загрузка...', // Text that is displayed during loading. Can contain %curr% and %total% keys
  gallery: {
    tPrev: 'Назад (Left arrow key)', // Alt text on left arrow
    tNext: 'Вперед (Right arrow key)', // Alt text on right arrow
    tCounter: '%curr% из %total%' // Markup for "1 of 7" counter
  },
  image: {
    tError: '<a href="%url%">The image</a> could not be loaded.' // Error message when image could not be loaded
  },
  ajax: {
    tError: '<a href="%url%">The content</a> could not be loaded.' // Error message when ajax request failed
  }
});

/*=====  End of Translating magnificPopup  ======*/



// Загрузка карты
function loadMapScript() {
  var script = document.createElement("script");
  script.src = "http://maps.googleapis.com/maps/api/js?callback=initializeMap";
  document.head.appendChild(script);
}

// Инициализация карты
function initializeMap() {
  var locationOffice = { lat: 53.256597, lng: 34.346457 };

  function createProp(defaultLocation) {
    return {
      center: defaultLocation,
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false,
    };
  }

  var mapProp = createProp(locationOffice);
  var map = new google.maps.Map(document.getElementById("ContactsMap"), mapProp);
  var markerPriem = new google.maps.Marker({
    position: locationOffice,
    map: map,
    title: 'Аварком-Центр'
  });


}



  function setMaxHeight(elements) {
    var maxHeight = 0;

    $(elements).each(function() {
      maxHeight = (maxHeight > this.clientHeight) ? maxHeight : this.clientHeight;
    }).innerHeight(maxHeight);
  }




  /*========================================
   =            HeroSlider            =
   ========================================*/

  var $heroSlider = $('.js-HeroSlider');
  if ($heroSlider.length) {
    $heroSlider.slick({
      accessibility: false,
      autoplay: true,
      prevArrow: '<button class="slick-prev"><svg class="slick-arrow__icon icon"><use xlink:href="#icon-arrow-prev"></use></svg></button>',
      nextArrow: '<button class="slick-next"><svg class="slick-arrow__icon icon"><use xlink:href="#icon-arrow-next"></use></svg></button>',
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            // dots: true
          }
        }
      ]
    });
  }

  /*=====  End of HeroSlider  ======*/



  /*==============================
   =            Popups            =
   ==============================*/
  var $openContactForm = $('.js-openPopupContactForm');
  $openContactForm.magnificPopup({
    focus: '.popup-contact-form .form-control',
    closeOnBgClick: false,
    // fixedContentPos: true,
    overflowY: 'scroll',
    removalDelay: 300,
    mainClass: 'mfp-fade'
  });

  /*=====  End of Popups  ======*/


  /*=====================================
   =            Scroll reveal            =
   =====================================*/

  window.sr = ScrollReveal({
    distance: '5em',
    duration: 600,
    viewFactor: 0.5
  });
  sr.reveal('.section-features .media-box', 90);
  sr.reveal('.section-how-work .media-box', {
    easing: 'ease-in-out',
    origin: 'right',
    rotate: { x: 0, y: 0, z: 15 },
  }, 130);
  sr.reveal('.section-advantages li', {
    // origin: 'top',
  }, 100);
  sr.reveal('.page-services .service', {
    origin: 'left'
  }, 90);

  /*=====  End of Scroll reveal  ======*/


  /*=============================
   =            Mmenu            =
   =============================*/

  $("#mobile-nav").mmenu();

  /*=====  End of Mmenu  ======*/



  /*====================================
   =            Contacts map            =
   ====================================*/
  //if ($('#ContactsMap').length) {
  //  window.onload = loadMapScript;
  //}
  /*=====  End of Contacts map  ======*/

  var url=location.href;

  $('.MainNav-list li a').each(function () {
    if (this.href == location.href) $(this).parent().addClass('MainNav-item-isActive');
  });

//  Формы


  $(".Popup-contact-form").submit(function (event) {

    event.preventDefault();
    var url="/zvonok";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'html',
      data: $(this).serialize()
    })
        .done(function (data) {

          $.magnificPopup.close();
          $(".Popup-contact-form, input[type=text], textarea").val("");
          swal("Письмо отправленно!", "Ближайшее время с вами свяжутся наши менеджеры!", "success");

        });



  });

  $(".FormCTA").submit(function (event) {


    event.preventDefault();
    var url="/consultant";
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'html',
      data: $(this).serialize()
    })
        .done(function (data) {


          $(".FormCTA, input[type=text]").val("");
          swal("Письмо отправленно!", "Ближайшее время с вами свяжутся наши менеджеры!", "success");

        });



  });





jQuery(function($) {

//  Формы
  $("#phone").mask("+9(999)999-99-99");
  $("#client_phone").mask("+9(999)999-99-99");
});
